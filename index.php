<?php
#print_r($_SERVER);
#die();
require_once('config.php');

foreach(glob("libs/*.php") as $class_filename)
	include_once($class_filename);
foreach(glob("ORM/*.php") as $class_filename)
	include_once($class_filename);
foreach(glob("models/*.php") as $class_filename)
	include_once($class_filename);

User::start();

if(!isset(Config::$routes))
	$routes = array();
else
	$routes = Config::$routes;

try {
	$router = Router::getInstance($routes);
	$router->getApp()->run($router->request);
} catch(Exception $e) { Error::except($e); }

Response::send();