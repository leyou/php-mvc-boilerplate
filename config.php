<?php
class Config {
	public static $database = array(
		'host'			=>	'localhost',
		'username'	=>	'root',
		'password'	=>	'',
		'database'	=>	'fiches'
	);

	public static $routes = array(
		#admin app
		'admin/login'							=>	array('app' => 'admin', 'controller' => 'login', 'action' => 'login'),
		'admin/:controller/:action/:id'		=>	array('app' => 'admin'),
		'admin/:controller/:action'			=>	array('app' => 'admin'),
		'admin/:controller'					=>	array('app' => 'admin', 'action' => 'index'),
		'admin'									=>	array('app' => 'admin', 'controller' => 'default', 'action' => 'index'),
		
		#default app
		'appartements/:id'					=>	array('controller' => 'appartement', 'action' => 'show'),
		'appartements'							=>	array('controller' => 'appartement', 'action' => 'index'),
		':controller/:action/:id'				=>	array(),
		':controller/:action'					=>	array(),
		':controller'								=>	array('action' => 'index'),
		''											=>	array('controller' => 'default', 'action' => 'index'),
	);
}