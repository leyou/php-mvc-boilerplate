<?php
class AppartementController extends Controller {
	public function index($request) {
		$this->appartements = ORM::getTable('Appartement')->query('SELECT * FROM %table% ORDER BY nom ASC');
	}

	public function show($request) {
		$id = $request['id'];
		try {
			$this->appartement = new Appartement($id);
		} catch(Exception $e) { $this->forward404(); }
	}
}