<?php
class ErrorController extends Controller {
	public function configure() {
		#$this->setLayout(false);
	}

	public function error404($request) {
		Response::setCode(404);
	}
}