<?php
class AdminApp extends App {
	public function configure($request) {
		if($request['controller'] != 'login' && User::getRole() != 'admin')
			Response::setCode(401)
				->setHeader('Location', "/fiches/admin/login")//TODO
				->send();
	}
}