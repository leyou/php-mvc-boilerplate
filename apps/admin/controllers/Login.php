<?php
class LoginController extends Controller {
	public function login($request) {
		if(isset($_POST['username'])) {
			User::setId(0);
			User::setRole('admin');
			Response::setHeader('Location', '/fiches/admin')->send();
		}
		$this->setLayout(false);
	}
	
	public function logout($request) {
		User::logout();
		Response::setHeader('Location', '/fiches/')->send();
	}
}