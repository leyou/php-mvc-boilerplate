<?php
class User {
	private static $id = null;
	private static $role = null;
	  
	public static function start() {
		session_start();
		if(isset($_SESSION['id']) && is_numeric($_SESSION['id']))
			self::$id = $_SESSION['id'];
		if(isset($_SESSION['role']))
			self::$role = $_SESSION['role'];
	}
	  
	public static function getId() {
		return self::$id;
	}
	  
	public static function setId($id) {
		$_SESSION['id'] = $id;
		self::$id = $id;
	}
	  
	public static function getRole() {
		return self::$role;
	}
	  
	public static function setRole($role) {
		$_SESSION['role'] = $role;
		self::$role = $role;
	}
	
	public static function logout() {
		unset($_SESSION['id']);
		self::$id = null;
		unset($_SESSION['role']);
		self::$role = null;
	}
	
	public static function unauthorized() {
		Response::setCode(401)
			->setHeader('Location', "/myproject/login")//TODO
			->send();
	}
}