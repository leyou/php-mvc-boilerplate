<?php
abstract class Model {
	public function __construct($param='') {
		$this->configure();

		if(is_array($param))
			$this->loadFromArray($param);
		else
			$this->loadFromID($param);
	}
	
	public function getTableName() {
		return strtolower(get_class($this)).'s';
	}
	
	public function loadFromID($id) {
		$cols = Database::getInstance()->query('SELECT * FROM '.$this->getTableName().' WHERE id=?', array($id))->fetchOne();
		if(!$cols)
			throw new Exception('Model not found');
		$this->loadFromArray($cols);
	}
	
	public function loadFromArray($cols) {
		foreach($cols as $col=>$value)
			$this->$col = $value;
	}
	
	public function save() {
	}
}