<?php
class Database {
	private $db;
	private static $instance;

	public function __construct() {
		$config = Config::$database;
		$this->db = mysql_connect($config['host'], $config['username'], $config['password']);
		mysql_select_db($config['database'], $this->db);
	}

	public function update($model, $conditions, $values) {
		unset($values['id']);
		$args = array();

		$sql = 'UPDATE '.$model.' SET ';

		foreach($values as $key=>$value) {
			$arr[] = '`'.$key.'`=?';
			$args[] = $value;
		}
		$sql .= implode(', ', $arr).' WHERE ';

		$arr = array();
		foreach($conditions as $key=>$value) {
			$arr[] = '`'.$key.'`=?';
			$args[] = $value;
		}
		$sql .= implode(' AND ', $arr);

		return $this->query($sql, $args);
	}

	public function insert($model, $values) {
		foreach($values as $key=>$value) {
			$keysArr[] = '`'.$key.'`';
			$valuesArr[] = '?';
		}
		$keysStr = implode(', ', $keysArr);
		$valuesStr = implode(', ', $valuesArr);
		$sql = 'INSERT INTO '.$model.' ('.$keysStr.') VALUES ('.$valuesStr.')';
		return $this->query($sql, array_values($values));
	}

	public function query($sql, $args=array()) {
		$format_str = str_replace('?', "'%s'", $sql);
		foreach($args as $k=>$v)
			$args[$k] = mysql_real_escape_string($v, $this->db);
		if($args) {
			array_unshift($args, $format_str);
			$sql = sprintf(call_user_func_array('sprintf', $args), $args);
		}

		return new Query($this->db, $sql);
	}

	public function lastId() {
		return mysql_insert_id($this->db);
	}

	public static function getInstance() { 
		if(!self::$instance)	self::$instance = new self();

		return self::$instance; 
	} 
}

class Query {
	private $res;

	public function __construct($db, $sql) {
		$this->res = mysql_query($sql, $db) or die(mysql_error().'<br/>'."\n".$sql);
	}

	public function num() {
		return mysql_num_rows($this->res);
	}

	public function fetchOne() {
		$res = $this->fetchAll();
		if(!$res)
			throw new Exception('no result found');
		return $res[0];
	}

	public function fetchAll() {
		$results = array();
		while($c = mysql_fetch_assoc($this->res)) {
			foreach($c as $k=>$v)
				if(is_numeric($v))
					$c[$k] = intval($v);
			$results[] = $c;
		}
		return $results;
	}
}