<?php
class App {
	public function run($request) {
		$this->controller = Router::getInstance()->getController();
		try {
			$this->configure($request);
			$this->controller->configure($request);
		} catch(Exception $e) {}
		$this->controller->run($request);
	}
	
	public function forward404() {
		
	}
}