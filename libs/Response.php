<?php
class Response {
	private static $instance;
	private static $content;
	private static $code;
	private static $headers = array();

	public static function setCode($code) { 
		self::$code = $code;
		return self::getInstance();
	} 

	public static function getCode() { 
		return self::$code;
	} 

	public static function setHeader($header, $value) {
		self::$headers[$header] = $value;
		return self::getInstance();
	}

	public static function setContent($content) {
		self::$content = $content;
		return self::getInstance();
	}

	public function getContent() {
		return self::$content;
	}

	public static function send() {
		switch(self::$code) {
			case 500:
			header('HTTP/1.1 500 Internal Server Error');
			break;
			case 404:
			header('HTTP/1.1 404 Not Found');
			break;
			case 204:
			header('HTTP/1.1 204 No Content');
			break;
			case 201:
			header('HTTP/1.1 201 Created');
			break;
			case 401:
			header('HTTP/1.1 401 Unauthorized');
			break;
			default:
			header('HTTP/1.1 200 OK');
			break;
		}
		foreach(self::$headers as $k=>$v)
			header($k.': '.$v);
		echo self::$content;
		exit();
	}

	public static function getInstance() { 
		if(!self::$instance)	self::$instance = new self();

		return self::$instance;
	}
}