<?php
abstract class ORM {
	public function getTable($table) {
		$name = strtolower($table).'Table';
		$rtable = new $name();
		$rtable->model = strtolower($table);
		return $rtable;
	}
}

abstract class Table {
	public function query($sql, $args=array()) {
		$db = Database::getInstance();
		$tableName = strtolower($this->model).'s';
		$sql = str_replace('%table%', $tableName, $sql);
		$modelName = ucfirst(strtolower($this->model));
		
		$results = $db->query($sql, $args)->fetchAll();
		$models = array();
		foreach($results as $result) {
			$models[] = new $modelName($result);
		}
		return $models;
	}
	
	#public static function findById($id) {
	#	return Table::query('SELECT * FROM ? WHERE id=?', array('id'=>$id))->fetchOne();
	#}
}