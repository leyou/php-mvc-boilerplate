<?php
class Router {
	private static $instance;
	public static $request;

	private function __construct($routes) {
		$this->request = $this->getRequest($routes);
		
		$appName = strtolower($this->request['app']);
		try {
			include('apps/'.$appName.'/'.$appName.'.php');
			$appClassName = $appName.'App';
			$this->app = new $appClassName();
		} catch(Exception $e) {
			Response::setCode(404)->send();
		}
		
		$controllerName = ucfirst(strtolower($this->request['controller']));
		try {
			if(file_exists('apps/'.$appName.'/controllers/'.$controllerName.'.php')) {
				include('apps/'.$appName.'/controllers/'.$controllerName.'.php');
				$controllerClassName = $controllerName.'Controller';
				$this->controller = new $controllerClassName();
			}
			else {
				$this->request['controller'] = 'error';
				$this->request['action'] = 'error404';
				include('apps/'.$appName.'/controllers/Error.php');
				$this->controller = new ErrorController();
			}
		} catch(Exception $e) {
			$this->request['controller'] = 'error';
			$this->request['action'] = 'error404';
			include('apps/'.$appName.'/controllers/Error.php');
			$this->controller = new ErrorController();
		}
	}
	
	public function runAction($controller, $action) {
		$this->request['controller'] = $controller;
		$this->request['action'] = $action;
		
		$controllerName = ucfirst(strtolower($controller));
		include('apps/'.$this->request['app'].'/controllers/'.$controllerName.'.php');
		$controllerClassName = $controllerName.'Controller';
		$this->controller = new $controllerClassName();
		
		$this->controller->run($this->request);
	}
	
	public function getApp() {
		return $this->app;
	}

	public function getController() {
		return $this->controller;
	}

	/*public static function getView($controller, $action) {
		$appName = strtolower($this->request['app']);
		include('apps/'.$appName.'/views/'.$controller.'/'.$action.'.php');
	}*/

	public static function getRoot() {
		return dirname($_SERVER['SCRIPT_NAME']);
	}

	public static function getAddress() {
		return 'http://'.$_SERVER['SERVER_NAME'].self::getRoot();
	}

	private static function getRequest($routes) {
		$root_path = dirname($_SERVER['SCRIPT_FILENAME']).'/';
		$request_dir = implode('/',array_filter(explode('/', $_SERVER['DOCUMENT_ROOT'] . $_SERVER['REDIRECT_URL'])));
		$request = str_replace(trim($root_path, '/'), '', trim($request_dir, '/'));
		$request = trim($request, '/');
		$parameters = explode('/', $request);
		$query_options = parse_str($_SERVER['QUERY_STRING']);
		if(!$query_options) $query_options = array();

		foreach($routes as $route=>$options) {
			$results = array_merge(array(), $options);
			$route = trim($route, '/');
			$route_regex = '/^'.preg_replace('/:[a-zA-Z0-9-_]+/', '[a-zA-Z0-9-_]+', str_replace('/', '\/', $route)).'/';
			
			if(preg_match($route_regex, $request)) {
				$route_params = explode('/', $route);
				$results = array_merge($results, $query_options);

				for($i=0; $i<sizeof($route_params); $i++) {
					if(strpos($route_params[$i], ':')===0) {
						$key = substr($route_params[$i], 1);
						$results[$key] = $parameters[$i];
					}
				}
				
				if(!isset($results['action'])) {
					switch($_SERVER['REQUEST_METHOD']) {
						case 'POST':
						$results['action'] = 'create';
						break;
						case 'GET':
						$results['action'] = 'show';
						break;
						case 'DELETE':
						$results['action'] = 'destroy';
						break;
						case 'PUT':
						$results['action'] = 'update';
						break;
					}
				}
				break;
			}
		}
		if(!isset($results['app']))
			$results['app'] = 'default';
		
		$results['data'] = file_get_contents('php://input');
		return $results;
	}

	public static function getInstance($routes = array()) { 
		if(!self::$instance)	self::$instance = new self($routes);

		return self::$instance; 
	} 
}