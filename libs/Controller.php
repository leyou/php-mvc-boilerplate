<?php
class Controller {
	var $view = null;
	var $db;
	var $layout = 'layout';
	var $app;
	var $controller;
	var $action;
	
	public function __construct() {
		$this->db = Database::getInstance();
	}

	public function setLayout($layout) {
		$this->layout = $layout;
	}
	
	public function forward404() {
		Router::getInstance()->runAction('error', 'error404');
	
		/*if(file_exists('apps/'.$this->app.'/views/404.php'))
			include('apps/'.$this->app.'/views/404.php');
		Response::setCode(404)->send();*/
	}

	public function run($request) {  
		#call action
		$this->app = $request['app'];
		$this->controller = $request['controller'];
		$this->action = $request['action'];
		$app = $request['app'];
		$controller = $request['controller'];
		$action = $request['action'];
		
		if(!method_exists($this, $action)) {
			$this->forward404();
		}
		$this->$action($request);
		
		$this->showView($request);
	}
		
	public function showView($request) {
		$app = $request['app'];
		$controller = $request['controller'];
		$action = $request['action'];
		
		#Prepare for the view
		foreach($this as $key=>$value) {
			$$key = $value;#TODO, watchout keywords
		}

		$_SESSION['gt'] = $this->getView($app, $controller, $action);//TODO: YICK!
		function get_view_path() {
			return implode('/', array_filter(explode('/', Router::getRoot() .'/'. dirname($_SESSION['gt']))));
		}
		function element($element, $params = array()) {
			global $app;
			if(empty($app)) $app = 'default';//TODO: WTF?!
			include('apps/'.$app.'/elements/'.$element.'.php');
		}

		if(!Response::getContent()) {
			ob_start();
			include($this->getView($app, $controller, $action));
			$content = ob_get_clean();
		
			if($this->layout) {
				ob_start();
				include('apps/'.$app.'/views/'.$this->layout.'.php');//TODO $app...
				$content = ob_get_clean();
			}
			
			Response::setContent($content);
		}
		
		Response::send();
	}

	public function redirect($url) {
	//TODO
		header('Location: '.Router::getRoot().$url);
		exit();
	}

	public function useView($tp) {
		$this->view = $tp;
	}

	public function getView($app, $controller, $action) {	  
		if(empty($this->view))
			return 'apps/'.$app.'/views/'.$controller.'/'.$action.'.php';
		else
			return 'apps/'.$app.'/views/'.$controller.'/'.$this->view;
	}
	
	//To override
	public function configure() {}
}